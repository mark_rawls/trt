#!/usr/bin/env ruby

require 'quartz_torrent'
require 'ruby-progressbar'
require 'trollop'
require 'fileutils'

include QuartzTorrent
include FileUtils

# get our option set
opts = Trollop::options do
  opt :remove,
      'Remove the .torrent file afterwards',
      short: '-r'

  opt :port,
      'Port to listen on',
      short: '-p',
      default: 5555

  opt :directory,
      'Directory to download to',
      short: '-d',
      default: '.'
end

# ensure we have at least one torrent to download
if ARGV.count == 0
  raise "Must provide at least one torrent file/magnet url"
end

# switch variable/signal handler
running = true
interrupted = false
Signal.trap('SIGINT') do
  puts "Got C-c, shutting down"
  running = false
  interrupted = true
end

# progressbar holder
bars = {}
removeFiles = []
numDone = 0
total = ARGV.count

# initialize the client
peerclient = PeerClient.new(opts[:directory])
peerclient.port = opts[:port]

# add all of our torrents to the client
ARGV.each do |arg|
  if MagnetURI.magnetURI?(arg)
    peerclient.addTorrentByMagnetURI MagnetURI.new(arg)
  else
    peerclient.addTorrentByMetainfo Metainfo.createFromFile(arg)

    if opts[:remove]
      removeFiles << arg
    end
  end
end

# fire it up
peerclient.start

# event loop
while running do
  peerclient.torrentData.each do |infohash, torrent|
    name = torrent.recommendedName
    pct = (
      torrent.completedBytes.to_f/torrent.info.dataLength.to_f * 100.0
    ).floor

    if pct == 100 && bars[name] != nil
      bars[name].progress = pct
      bars[name] = nil
      numDone += 1
    end

    if bars[name] == nil && pct != 100
      bars[name] = ProgressBar.create(title: name)
    else
      next
    end

    bars[name].progress = pct
  end

  if numDone == total
    running = false
  end
end

peerclient.stop

# clear out torrent files set to remove
if !interrupted
  removeFiles.each {|f| rm(f)}
end
